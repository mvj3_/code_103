//异步获取api远程数据
public async Task<String> GetDataAsync(string uriString, string encoding)
{
	//初始化一个Uri对象
    Uri uri = new Uri(uriString);
    string _encoding = "utf8";

    try
    {
		//初始化HttpClient,这里主要采用HttpClient
        HttpClient client = new HttpClient();
		//获取响应uri地址的响应结果
        HttpResponseMessage response = await client.GetAsync(uri);
        response.EnsureSuccessStatusCode();
		//得到响应结果状态值
        Boolean statusCode = response.IsSuccessStatusCode;
		//判断网络是否连接成功
        if (!statusCode)
        {
            Windows.UI.Popups.MessageDialog dlg = new Windows.UI.Popups.MessageDialog("网络通信不畅");
            await dlg.ShowAsync();
            return string.Empty;
        }
		//以字节流的方式将结果读取
        Stream resStream = await response.Content.ReadAsStreamAsync();
        Encoding enc = Encoding.GetEncoding("gb2312");
        if(_encoding.Equals("utf8"))
        {
            enc = Encoding.UTF8;
        }
		//将获取到的流文件以指定的编码进行读取
        StreamReader sr = new StreamReader(resStream, enc);
		//读取所有内容
        String responseBody = sr.ReadToEnd();

        return responseBody;
    }
    catch (Exception)
    {
        return string.Empty;
    }
}